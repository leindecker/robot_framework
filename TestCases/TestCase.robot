*** Settings ***
Resource        ../Resources/Resource.robot
Suite Teardown  Fechar Navegador

*** Test Case ***
Pesquisar Produtos Existentes
    Acessar a página home do site
    Digitar o nome do produto "Blouse" no campo de pesquisa
    Clicar no botão de pesquisar
    O Sistema deve retornar o produto pesquisado
    Fechar Navegador

Pesquisar Produto Não Existente
    Acessar a página home do site
    Digitar o nome do produto "produtoNãoExistente" no campo de pesquisa
    Clicar no botão de pesquisar
    O Sistema deve exibira a mensagem "No results were found for your search "produtoNãoExistente""
