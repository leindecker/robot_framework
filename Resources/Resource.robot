*** Settings ***
Library     SeleniumLibrary

*** Variable ***
${BROWSER}                      Chrome
${URL}                          http://automationpractice.com/index.php
${BANNER}                       id:slider_row
${INPUT_PRODUTOS_PESQUISA}      id:search_query_top
${BOTAO_PESQUISAR}              css=.btn.button-search
${NOME_PRODUTO_PESQUISADO}      css=.page-heading span.lighter
${PRODUTO_PESQUISADO}           css=ul.product_list .product-name
${LISTA_PRODUTOS_PESQUISADOS}   css=.product-container
${TEXTO_PRODUTO_NAO_EXISTENTE}  css=.alert


*** Keywords ***
Acessar a página home do site
    Open Browser    ${URL}  ${BROWSER}
    Maximize Browser Window
    Wait Until Element Is Visible   ${BANNER}
    Title Should Be     My Store

Digitar o nome do produto "${NOME_PRODUTO}" no campo de pesquisa
    Input Text      ${INPUT_PRODUTOS_PESQUISA}    ${NOME_PRODUTO}

Clicar no botão de pesquisar
    Click Element   ${BOTAO_PESQUISAR}

O Sistema deve retornar o produto pesquisado
    Wait Until Element Is Visible   ${PRODUTO_PESQUISADO}
    Element Text Should Be    ${PRODUTO_PESQUISADO}     Blouse

O Sistema deve exibira a mensagem "${MENSAGEM_PRODUTO_INEXISTENTE}"
    Element Text Should Be    ${TEXTO_PRODUTO_NAO_EXISTENTE}     ${MENSAGEM_PRODUTO_INEXISTENTE}

Fechar Navegador
    Close Browser